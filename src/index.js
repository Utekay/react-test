import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      remotePostList: [
      ],
      localPostList: [
      ]
    }
  }

  componentDidMount () {
    fetch('http://jsonplaceholder.typicode.com/posts')
      .then(response => {
        if (response.ok) {
          return response.json();
        }
      })
      .then(json => this.setState({
        remotePostList: json
      }))
  }

  handleDragStart (id, ev) {
    ev.dataTransfer.setData('id', id)
  }

  handleDragOver (ev) {
    ev.preventDefault();
  }

  handleDrop (ev) {
    ev.preventDefault();
    const id = ev.dataTransfer.getData('id');
    if ( !id ) {
      return
    }

    let localPostList = this.state.localPostList;
    let remotePostList = this.state.remotePostList;

    remotePostList = remotePostList.filter( (item) => {
      if ( item.id == id ) {
        localPostList.push(item);
        return false;
      }
      return true;
    });

    this.setState({
      localPostList: localPostList,
      remotePostList: remotePostList
    });
  }

  render () {
    return (
      <div className='app'>
        <div className='block'>
          <ul className='post-list'>
            {this.state.remotePostList.map((item) =>
              <PostItem key={item.id}
                title={item.title}
                body={item.body}
                draggable={true}
                handleDragStart={this.handleDragStart.bind(this, item.id)}
              />
            )}
          </ul>
        </div>
        <div className='block'
            onDrop={this.handleDrop.bind(this)}
            onDragOver={this.handleDragOver}>
          <ul className='post-list'>
            {this.state.localPostList.map((item) =>
              <PostItem key={item.id}
                  title={item.title}
                  body={item.body}
              />
            )}
          </ul>
          <DownloadButton items={this.state.localPostList} />
        </div>
      </div>
    )
  }
}

class PostItem extends React.Component {
  render () {
    return (
      <li className='post-list__item'
          draggable={this.props.draggable ? true : false}
          onDragStart={this.props.handleDragStart}>
        <h2>{this.props.title}</h2>
        <p>{this.props.body}</p>
      </li>
    )
  }
}

class DownloadButton extends React.Component {
  getDataUrl () {
    let data = 'data:text/json;charset=utf-8,';
    data += encodeURIComponent(JSON.stringify(this.props.items));
    return data;
  }

  render () {
    if ( !this.props.items.length ) {
      return null;
    }
    return (
      <a href={this.getDataUrl()}
          download='json.json'
          className='download'>
        Скачать
      </a>
    )
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);


module.hot.accept();
